set(CMAKE_AUTOMOC ON)

# As moc files are generated in the binary dir, tell CMake
# to always look for includes there:
set(CMAKE_INCLUDE_CURRENT_DIR ON)

find_package(Qt5Test REQUIRED)

add_executable(foo foo.cpp)

target_link_libraries(foo PRIVATE Qt5::Test)

add_test(footest foo)
