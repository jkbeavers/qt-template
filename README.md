# Simple Qt Hello World Using CMake
This is a basic repo building Qt using CMake. Trying to follow [modern Cmake
practices](https://cliutils.gitlab.io/modern-cmake/).

This depends on
- Qt5
- C++ 14
- CMake > 3.5.0

## Build
`./build.sh [-t]`

## Run
`bin/helloworld`
