#!/bin/bash

# Always run from script location (top of repo)
base_dir=`dirname ${0}`
pushd ${base_dir}

  #########################
  # Parse Options and Setup
  #########################
  build_dir="builds"
  compile_commands="on" # or off
  
  print_usage() {
      printf "Usage: -[t(test)]\n"
  }
  test_flag='false'
  
  while getopts 't' flag; do
    case "${flag}" in
      t) test_flag='true' ;;
      *) print_usage
         exit 1 ;;
    esac
  done
  
  #########################
  # Build and Test
  #########################
  cmake -S . \
	  -B${build_dir} \
	  -DBUILD_TESTING=${test_flag} \
      -DCMAKE_EXPORT_COMPILE_COMMANDS=${compile_commands} \
	  && \
    pushd ${build_dir} && \
    make
  if [[ ${test_flag} ]]; then
    make test
  fi
  popd

popd
