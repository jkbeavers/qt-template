#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLabel>
#include <QLineEdit>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  auto edit = new QLineEdit();
  edit->setText("Hello World");

  setCentralWidget(edit);
}

MainWindow::~MainWindow() { delete ui; }
